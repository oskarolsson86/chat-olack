import React from 'react'
import io from 'socket.io-client'

// boostrap components
import * as Bootstrap from "react-bootstrap";

//Style
import './dashboard.css'

const socket = io('http://localhost:4001')

class Dashboard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            messages: ['test', 'test2', 'test4'],
            message: '',
            newMessages: false,
            user: null,
            loggedInUsers: [],
            color: 'red'

        }
        socket.on("message", (mes) => {
                    console.log('running ' , mes);
                    this.addMessage(mes); 
                })
        socket.on("addUser", user => {
            const concatUsers = this.state.loggedInUsers.concat(user);
            this.setState({
                loggedInUsers: concatUsers
            })
        })
    }

    componentDidMount() {
        const urlParams =  new URLSearchParams(window.location.search);
        // shitty solution to see if user is logged in
        if (urlParams.get('name')) {
            socket.on('connect', () => {
              });
              this.setState({
                  user: urlParams.get('name'),
                  picture: urlParams.get('picture')
              })
        } else {
            this.props.history.push('/login')
        }
    }

 
    addMessage = (data) => {
        let usernamePlusMsg = data.user + ': ' + data.msg;
        this.setState({
            messages: [...this.state.messages, usernamePlusMsg],
            color: data.color
            
        })
    }

    sendMessage = () => {
        socket.emit('message', {msg : this.state.message, user: this.state.user, color:this.state.color})
 
    }

    handleChange = (event) => {
        this.setState({message: event.target.value});
      }

      handleColor = (event) => {
          this.setState({ color: event.target.value})
      }


    render() {
        const { messages, message, color } = this.state;
        return (
            <div className="dashboard">
                 <div className="chatbox">
                     
                {
                    messages.map( (msg, i) => {
                        return (
                        <div className="msgRow" key={i}>
                            <Bootstrap.ListGroup.Item style={{color: this.state.color}}> {msg} </Bootstrap.ListGroup.Item>
                      
                        </div>
                        )
                        
                    })
                }
                
                    <div className="typeBox">
                        <Bootstrap.InputGroup  className="mb-3">
                            <Bootstrap.InputGroup.Prepend>
                            <Bootstrap.InputGroup.Text id="inputGroup-sizing-default" > <img width="25px" src={this.state.picture} alt="profile pic"/> </Bootstrap.InputGroup.Text>
                            </Bootstrap.InputGroup.Prepend>
                            <Bootstrap.FormControl aria-label="Default" aria-describedby="inputGroup-sizing-default" value={message} onChange={this.handleChange}/>  
                            <Bootstrap.Button variant="info" onClick={this.sendMessage}>Snd</Bootstrap.Button>
                        </Bootstrap.InputGroup>
                    </div>
       
                </div>
               <div>    <Bootstrap.InputGroup>
                        <Bootstrap.FormControl aria-describedby="inputGroup-sizing-dd" value={color} onChange={this.handleColor}/> 
                  </Bootstrap.InputGroup> Logged in users:
                   {
                       this.state.loggedInUsers.map( (user, i) => {
                           return <p key={i} >{user}</p>
                       })
                   }
               </div>
            </div>
            
        )
    }
}

export default Dashboard;