import React from 'react'

// Components
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'

//STyle
import './login.css'

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            superSecretClientId: '221918101508-ce3tkql6s6kc810mnp7crej1hr1svi4l.apps.googleusercontent.com'
        }
    }

    handleUserChange = (e) => {
        this.setState({
            username: e.target.value
        })
    }

    submitUserName =  () => {
      
      
    }

    signInWithGoogle = () => {
        const redirectUri = 'http://localhost:4001/oauth'
        const scope = 'profile email openid'
        const responseType = 'code'

        window.location.href = `https://accounts.google.com/o/oauth2/v2/auth?client_id=${this.state.superSecretClientId}&redirect_uri=${redirectUri}&scope=${scope}&response_type=${responseType}&access_type=offline&include_granted_scopes=true`
    }

    render() {

        return (
            <div className="container">
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Type username" value={this.state.username} onChange={this.handleUserChange} />
                        <Form.Text className="text-muted">
                            Sign in and chat with yourself
                        </Form.Text>
                    </Form.Group>

                   
                    <Button variant="primary" onClick={this.submitUserName}>
                        Start chat
                    </Button>

                    <Button variant="primary" onClick={this.signInWithGoogle}>
                        Sign in with Google
                    </Button>
                </Form>
            </div>
        )
    }
}
export default Login;