import React from 'react';
import { 
  BrowserRouter as Router,
  Route
 } from 'react-router-dom';

// Components
import Dashboard from './views/Dashboard';
import Login from './views/Login';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/dashboard" component={Dashboard}/>
        <Route path="/login" component={Login}/>
      </Router>
    </div>
  );
}

export default App;
